/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.2.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      photoBioProperties;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

photoBio          on;
photoBioModel     photoBioDOM;
photoBioDOMCoeffs
{
    nPhi          8;      // azimuthal angles in PI/2 on X-Y.(from Y to X)
    nTheta        8;      // polar angles in PI (from Z to X-Y plane)
    convergence   5e-6;   // convergence criteria for radiation iteration
    maxIter       50;    // maximum number of iterations
    nBand         1;   // This simulation uses two bands, each with peak 800 and 850
    nPixelPhi	  5;
    nPixelTheta   5;
    phaseFunctionModel schlickModel;
    schlickModelCoeffs
    {
        subAngleNum     1;
        asymmetryFactor	(0.98 );
        inScatter       true;
    }
}

// Number of flow iterations per radiation iteration
solverFreq 1;

extinctionModel	wideBandVariableExtinction;
wideBandVariableExtinctionCoeffs
{
	nBands          1;
	absorption      true;
	nAbsorbing      1;
	absorbingVars   (XPB); // add waterFrac, k\850 = 2.93e-7
	absorptionCoeff	((106.47)); // Berberoglu, Pilon (2007)
	scattering      true;
	nScattering     2;
	scatteringVars  (XPB XS XI);
	scatteringCoeff ((18.669) (18.669) (18.669));
}

// ************************************************************************* //
