/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  5                                     |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      biokineticsProperties;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvePam     true;
solveFlow    false;
solveRad     true;

stoichiometry
{
    YPBPH        1.000000;
    YPBCH        0.680706;
    fSSXS        0.163824;
    fSACXS       0.166839;
    fSH2XS       8.442468E-02;
    fSINXS       0.011622;
    fSIPXS       0.002835;
    fSIXS        0.1518209;
    fXIXS        0.4330911;
    fNB          0.086000;
    fPB          0.015000;
    fSACCH       0.669100;
    fSH2CH       0.330900;
    fSICAU       1.000000;
    fINDEC       0.058000;
    fIPDEC       0.010000;
    YPBAU        YPBAU    [1 0 0 0 -1 0 0]  0.04032;
    fSICXS       fSICXS   [-1 0 0 0 1 0 0]  1.30397;
    fSH2AU       fSH2AU   [1 0 0 0 -1 0 0]  0.04032;
    fSICAC       fSICAC   [-1 0 0 0 1 0 0]  6.448413;
    fSICSS       fSICSS   [-1 0 0 0 1 0 0] -1.24276;
    fICDEC       fICDEC   [-1 0 0 0 1 0 0] -0.1984;
}

kinetics
{
    muPH        muPH  [ 0 0 -1 0 0 0 0 ] 1.6609E-5;
    muAC        muAC  [ 0 0 -1 0 0 0 0 ] 2.7488E-5;
    muCH        muCH  [ 0 0 -1 0 0 0 0 ] 8.5648E-7;
    muAU        muAU  [-1 0 -1 0 1 0 0 ] 6.9444E-5;
    kd          kd    [ 0 0 -1 0 0 0 0 ] 1.0417E-6;
    kh          kh    [ 0 0 -1 0 0 0 0 ] 8.2060E-7;
    KSS         KSS   [ 1 -3 0 0 0 0 0 ] 5.2424E-4;
    KSAC        KSAC  [ 1 -3 0 0 0 0 0 ] 2.0223E-2;
    KSH2        KSH2  [ 1 -3 0 0 0 0 0 ] 1.0000E-9;
    KSIN        KSIN  [ 1 -3 0 0 0 0 0 ] 2.0000E-5;
    KSIC        KSIC  [ 0 -3 0 0 1 0 0 ] 0.42;
    KFA         KFA   [ 1 -3 0 0 0 0 0 ] 7.8500E+0;
    KSIP        KSI   [ 1 -3 0 0 0 0 0 ] 8.1500E-5;
    KG          KG    [ 1 0 -3 0 0 0 0 ] 8.7600E+0;
}

diffusivities
{
    DSS         DSS    [ 0 2 -1 0 0 0 0] 1e-6;
    DSAC        DSAC   [ 0 2 -1 0 0 0 0] 1e-6;
    DSH2        DSH2   [ 0 2 -1 0 0 0 0] 1e-6;
    DSIC        DSIC   [ 0 2 -1 0 0 0 0] 1e-6;
    DSIN        DSIN   [ 0 2 -1 0 0 0 0] 1e-6;
    DSIP        DSIP   [ 0 2 -1 0 0 0 0] 1e-6;
    DSI         DSI    [ 0 2 -1 0 0 0 0] 1e-6;
    DXI         DXI    [ 0 2 -1 0 0 0 0] 1e-5;  //1e-9
    DXPB        DXPB   [ 0 2 -1 0 0 0 0] 1e-5;  //1e-9
    DXS         DXS    [ 0 2 -1 0 0 0 0] 1e-5;  //1e-9
}

oxygenTransferModel
{
      type constant;
      constantCoeffs
      {
          kLa    kLa    [0 0 -1 0 0 0 0] 0.00243;
      }
}


SOsat    SOsat [1 -3 0 0 0 0 0] 0.00928;

ScT           0.7;
liquidName    water;
gasName       air;

// ************************************************************************* //
